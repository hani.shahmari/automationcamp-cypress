/// <reference types="cypress"/>

describe('Suite 1' , () => {
    it( '1- Google Search', function(){
        cy.visit("https://www.google.com")
        cy.get('#L2AGLb').click()
        cy.get('#APjFqb').type("Cypress{enter}")
    
    })
    
    it('2- Cypress Website',() => {
        cy.visit("https://cypress.io")
        cy.title().should('eq', 'JavaScript Component Testing and E2E Testing Framework | Cypress')
        cy.title().should('include', 'JavaScript Component Testing and E2E Testing Framework')
    })
    
})

// npx cypress run cypress/e2e/session1.cy.js

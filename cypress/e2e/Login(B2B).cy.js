describe('Suite 1 => Login', () => {
    it('1- Login with CAPTCHA', function () {
        cy.visit("https://b2bcare.mobinnet.ir/#/auth/login");
        cy.get('.input-top-round > .form-control').type("admin");
        cy.get(':nth-child(2) > .form-control').type("Admin1234");

        // Find and capture the CAPTCHA image element
        cy.get('.input-bottom-round > .captcha > img').screenshot('captcha').then(() => {
            // Confirm that the screenshot is saved in the expected location
            cy.readFile('./cypress/screenshots/captcha.png', 'base64').then((captchaImage) => {
                const apiEndpoint = 'http://2captcha.com/in.php';
                const apiKey = 'your_2captcha_api_key'; // Replace with your actual API key
                const pollInterval = 5000;
                const maxRetries = 10; // Maximum number of retries

                // Function to poll for CAPTCHA solution
                const pollForCaptchaSolution = (captchaID, retryCount = 0) => {
                    if (retryCount >= maxRetries) {
                        throw new Error('CAPTCHA solving retries exceeded');
                    }

                    cy.wait(pollInterval).request({
                        method: 'GET',
                        url: `http://2captcha.com/res.php?key=${apiKey}&action=get&id=${captchaID}&json=1`,
                    }).then((captchaResponse) => {
                        if (captchaResponse.body.includes('OK')) {
                            const captchaSolution = captchaResponse.body.split('|')[1];

                            // Enter the CAPTCHA solution into the input field (inspect the website to determine the input field)
                            cy.get('#captcha-input').type(captchaSolution);

                            // Submit the form or perform other actions
                            // ...

                            // Assert that the login was successful or the desired action was performed
                            // ...
                        } else {
                            // Retry polling
                            pollForCaptchaSolution(captchaID, retryCount + 1);
                        }
                    });
                };

                // Upload CAPTCHA image to 2Captcha
                cy.request({
                    method: 'POST',
                    url: apiEndpoint,
                    timeout: 120000,
                    form: true,
                    body: {
                        key: apiKey,
                        method: 'base64',
                        body: `data:image/png;base64,${captchaImage}`,
                    },
                }).then((response) => {
                    if (response.body.includes('OK')) {
                        const captchaID = response.body.split('|')[1];

                        // Start polling for CAPTCHA solution
                        pollForCaptchaSolution(captchaID);
                    } else {
                        throw new Error('CAPTCHA upload failed');
                    }
                });
            });
        });

        // Click the login button
        cy.get('.btn').click();
    });
});

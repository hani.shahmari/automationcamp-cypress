/// <reference types="cypress"/>

// describe('Suite', function () {
// it('test1', function () {
//     cy.visit('https://play2.automationcamp.ir/')
//     cy.get('#fname').type('hanii')
// })

// it('Scroll rows', ()=>{
//     cy.visit('https://datatables.net/examples/basic_init/scroll_x.html')
//     cy.get('tbody > :nth-child(7) > :nth-child(9)').click()
// })

// it('contains', () => {
//     cy.visit('https://play2.automationcamp.ir/')
//     cy.contains('This is your form title:')
//     cy.contains("label[for= 'moption']" , "Option 2")
//     cy.get("label[for= 'moption']").contains('Option 3')
// })

// it('children', () => {
//     cy.visit('https://play2.automationcamp.ir/')
//     cy.get("#owc").children("[value='option 1']").click()
//     cy.contains("Singer").parent()
//     cy.get("#owc").children("[value='option 3']").siblings().should('have.length' , 3) //هم جوار خودش رو مشخص میکنه
// })

// it('ancestors', () => {
//     cy.visit('https://en.wikipedia.org/wiki/Main_Page')
//     cy.get("#mp-welcome").parents()
//     cy.get("#mp-welcome").parents("#mw-content-text")
//     cy.get("#mp-welcome").parentsUntil("#bodycontent")
// })

// it('descendants', () => {
//     cy.visit('https://en.wikipedia.org/wiki/Main_Page')
//     cy.get("#bodyContent").find("#mw-content-text")
//     cy.get("#bodyContent").within( () => {
//         cy.get("#mw-content-text")
//     })
// })

// it('index', () => {
//     cy.visit('https://play2.automationcamp.ir/')
//     cy.get("td").eq(5)
//     cy.get("td").first()
//     cy.get("td").last()
// })

// it('filter', () => {
//     cy.visit('https://play2.automationcamp.ir/')
//     cy.get("td").filter('#td_id')
//     cy.get("td").not('#td_id')
// })

it('traversal', () => {
    cy.visit('https://play2.automationcamp.ir/')
    cy.get("#fname").closest('div').should('have.class', 'main')
    cy.get("[value='td1_value']").next()
    cy.get("[value='td1_value']").nextAll()
    cy.get("[value='td1_value']").nextUntil("[value='td4_value']")
    cy.get("[value='td4_value']").prev()
    cy.get("[value='td4_value']").prevAll()
    cy.get("[value='td4_value']").prevUntil("[value='td1_value']")

})
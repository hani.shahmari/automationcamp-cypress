/// <reference types="cypress"/>

// describe('session3', function () {
// it('1- keyboard actions > Type', function () {
//     cy.visit("https://www.google.com/")
//     cy.get("#APjFqb").type("{enter} آموزش سایپرس")
// });

// it('2- keyboard actions > using sequences', function () {
//     cy.visit("https://www.google.com/")
//     // cy.get("#APjFqb").type("Cypress").type("{backspace}").type("{HOME}").type("{del}")
//     cy.get("#APjFqb").type("Cypress").type("{backspace}{home}{del}")
// });

// it('3- keyboard actions > combination', function () {
//     cy.visit("https://www.google.com/")
//     // cy.get("#APjFqb").type("Cypress").type("{backspace}").type("{HOME}").type("{del}")
//     cy.get("#APjFqb").type("Cypress")
//     .realPress(['Control','A'])
//     .realPress(['Control','X'])
//     .realPress(['Control','V'])
// });

// it('4- keyboard actions > combination', function () {
//     cy.visit("https://www.google.com/")
//     cy.get("#APjFqb").type("Cypress").clear()
//     cy.get("#APjFqb").type("Cypress2")
//     cy.get("#APjFqb").type("{selectall}{backspace}")
// });

// it('4- keyboard actions > delay option', function () {
//     cy.visit("https://www.google.com/")
//     cy.get("#APjFqb").type("I'm typing slowly", {delay:100})
//     cy.get("#APjFqb").clear()
//     cy.get("#APjFqb").type("I'm typing slowly", {delay:0})
// });

// it('5- keyboard actions > Repeat', function () {
//     cy.visit("https://www.google.com/")
//     cy.get("#APjFqb").type("I'm typing slowly ".repeat(4))
// });

// })

// describe('session3 - Mouse Action', function () {

    // it('click / DB click - Right click', function () {
    // cy.visit('https://play2.automationcamp.ir/')
    // cy.get('input#male').click()
    // cy.get('input#male').should('be.checked')
    // //Double Click
    // cy.contains("Double-click me").dblclick()
    // cy.contains("Your Sample Double Click worked!")
    // //Right click
    // // cy.get('body').rightclick("top")
    // // cy.get('[href="contact.html"]').click({ctrlKey:true})
    // cy.get('body').rightclick({force:true})
    // });

    // it('Mouse Hover 1', function(){
    //     cy.visit("test-hover.html")
    //     cy.get('#left11').should('not.be.visible')
    //     cy.get('#menu1').trigger('mouseover')
    //     cy.get('#left11').should('be.visible')

    //     cy.get('#left21').should('not.be.visible')
    //     cy.get('#menu2').realHover()
    //     cy.get('#left21').should('be.visible')
    // });

    // it('Long press (Click and Hold)', function () {
    //     cy.visit("https://demos.telerik.com/kendo-ui/circular-gauge/index", { timeout: 30000 })
    //     // cy.get("[role=button][title=Increase]", { timeout: 30000 }).trigger("mousedown", { whitch: 1 })
    //     cy.get("[role=button][title=Increase]", { timeout: 30000 }).realMouseDown()
    //         .wait(70000)
    //         .trigger("mouseup", { force: true })
    //     cy.get("[role=slider]").invoke("attr", 'aria-valuenom')
    //     .should('not.equal', '55')
    // });

    // it('Drag and drop', function () {
    //     cy.visit("https://selenium08.blogspot.com/2020/01/drag-drop.html")
    //     cy.get("#draggable")
    //     .trigger("mousedown", {which: 1})
    //     .get("#droppable")
    //     .trigger("mousemove")
    //     .trigger("mouseup", {force: true})    
    // });

    // it('Drag and drop by offset', function () {
    //     cy.visit("https://selenium08.blogspot.com/2020/01/drag-drop.html")
    //     cy.get("#draggable")
    //     .trigger("mousedown", {which: 1})
    //     .realMouseMove(300,100)
    //     .realMouseUp()
    // });
// });

describe('Session3 - Scroll', function () {
    // topLeft, top, topRight, left, center, right, bottomLeft, bottom, and bottomRight
    it('1-Scroll Page - To Position', function () {
        cy.visit("https://www.imdb.com/chart/top/")
        cy.scrollTo('bottom')
    });
    it('2-Scroll Page - By Coordination', function () {
        cy.visit("https://www.imdb.com/chart/top/")
        cy.scrollTo(0, 1300)
    });
    it('3-Scroll Page - By Pixel', function () {
        cy.visit("https://www.imdb.com/chart/top/")
        cy.scrollTo('0px', '3000px')
    });
    it('4-Scroll Page - By Percentage', function () {
        cy.visit("https://www.imdb.com/chart/top/")
        cy.scrollTo('0%', '60%')
    });
    it('5-Scroll Element Into View', function () {
        cy.visit("https://www.imdb.com/chart/top/")
        cy.get(':nth-child(245) > .titleColumn').scrollIntoView()
    });
    it('6-Scroll Element - To Position', function () {
        cy.visit("https://datatables.net/examples/basic_init/scroll_xy.html")
        cy.scrollTo("100%", "10%")
        cy.get(".dataTables_scrollBody").scrollTo('right')

    });
    it('7-Scroll Element - By Coordination', function () {
        cy.visit("https://datatables.net/examples/basic_init/scroll_xy.html")
        cy.scrollTo("100%", "10%")
        cy.get(".dataTables_scrollBody").scrollTo(300, 400)
    });
    it('8-Scroll Element - By Percentage', function () {
        cy.visit("https://datatables.net/examples/basic_init/scroll_xy.html")
        cy.scrollTo("100%", "10%")
        cy.get(".dataTables_scrollBody").scrollTo('50%', '50%')
    });
    it('9-Scroll with Duration', function () {
        cy.visit("https://datatables.net/examples/basic_init/scroll_xy.html")
        cy.scrollTo("100%", "10%")
        cy.get(".dataTables_scrollBody").scrollTo('center', {duration: "2000"})
    });
    it('10-Scroll with Line Easing', function () {
        cy.visit("https://datatables.net/examples/basic_init/scroll_y.html")
        cy.scrollTo("100%", "10%")
        cy.wait(500)
        // cy.get(".dataTables_scrollBody").scrollTo('center', {duration:500, easing: 'swing' })
        cy.get(".dataTables_scrollBody").scrollTo('center', {duration:500, easing: 'linear' })
    });

});
